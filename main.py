__author__ = 'dhondo01'
import os
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from pprint import pprint
import random
app = Flask(__name__)
app.config['SECRET_KEY'] = "42"
app.debug=True

wordList=[]


f = open('words.txt','r')
for word in f: wordList.append((word.replace('\n',''),len(word)))
f.close()

# controllers

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.route('/')
def xkcd_page():
    return render_template('xkcd.html')

@app.route('/Passwords/', methods=["post"])
def Passwords():
    var = 10
    shortestWord = int(request.form['shortestWord'])
    longestWord = int(request.form['longestWord'])
    totalLength = int(request.form['totalLength'])
    changeDict = {}
    capList = [False,False,False,False]
    if 'eSub' in request.form.keys():
        changeDict['i'] = '!'
    if 'oSub' in request.form.keys():
        changeDict['o'] = '0'
    if 'sSub' in request.form.keys():
        changeDict['s'] = '5'
    if 'aSub' in request.form.keys():
        changeDict['a'] = '@'
    if 'firstCap' in request.form.keys():
        capList[0] = True
    if 'secondCap' in request.form.keys():
        capList[1] = True
    if 'thirdCap' in request.form.keys():
        capList[2] = True
    if 'fourthCap' in request.form.keys():
        capList[3] = True

    passwordOptions = []
    for i in range(10):
        wordsList = sortingHat(filterWords(shortestWord,longestWord),totalLength)
        if not wordsList:
            flash("Not a valid decimal value", "error")
            return redirect('error')
        obfuscatedWord = blender(wordsList,capList,changeDict)
        passwordOptions.append(obfuscatedWord)

    for thing in request.form.keys():
        print(thing,request.form[thing])
    
    return render_template('passwords.html',passwordList = passwordOptions)

def filterWords(shortest, longest):
    def f(x): return shortest <= x[1] <= longest and "'" not in x[0]
    sorted_list =list(filter(f,wordList))
    sorted_list.sort(key=lambda tup: tup[1])
    return list(sorted_list)

def sortingHat(choices, maxLength):
    try:
        currentLength = 0
        firstChoice = random.choice(choices)
        currentLength+=firstChoice[1]
        secondChoice = random.choice(choices)
        currentLength+=secondChoice[1]
        thirdChoice = random.choice(choices)
        currentLength+=thirdChoice[1]
        def f(x): return x[1] == (maxLength - currentLength)
        def g(x): return x[1] == (maxLength - currentLength)//2
        siftedWords= list(filter(f,choices))
        if len(siftedWords)==0:
            currentLength-=thirdChoice[1]
            reSiftedChoices = list(filter(g,choices))
            thirdChoice = random.choice(reSiftedChoices)
            currentLength+=thirdChoice[1]
            siftedWords = list(filter(f,choices))
        fourthChoice = random.choice(siftedWords)
        currentLength+=fourthChoice[1]
        return [firstChoice[0],secondChoice[0],thirdChoice[0],fourthChoice[0]]
    except:
        return False

def blender(wordList, capList, changeDict):
    obfWord = ""
    for idx in range(4):
        word = wordList[idx]
        toCap = capList[idx]
        newWord = ""
        for char in word:
            if char.lower() in changeDict:
                char = changeDict[char.lower()]
            newWord = newWord+char
        if toCap:
            newWord = newWord.capitalize()
        obfWord = obfWord + " " + newWord + " "
    return obfWord



if __name__ == '__main__':
    app.secret_key = "42"
    app.run()
